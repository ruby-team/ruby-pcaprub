require 'rake/testtask'

unless ENV['ADTTMP'].nil?
  Rake::TestTask.new(:default) do |test|
    test.libs << 'test'
    test.pattern = 'test/**/test_*.rb'
    test.verbose = true
  end
  task :default => [:test]
else
  $stderr.puts "Not running in autopkgtest. Skipping tests."
  task :default => []
end
